# Terraform DiagAndGo

README for the infra of  **DiadAndGo**. Everything in this repo is related to the **Terraform** infrastructure.

## First of all
Install the AWS cli: https://docs.aws.amazon.com/fr_fr/cli/latest/userguide/installing.html <br>
Install the Terraform CLI: https://www.terraform.io/intro/getting-started/install.html <br>
Setup your credentials with `aws configure` <br>

## How it works

For launch / update / delete the infra you need only modifications in the configurations files and some CLI commands.<br>
Every commands have to be launched in the `terraform` directory.<br>

This schema explain the steps for launching/deleting the infra.<br>

```mermaid
graph LR
A(1-Init) --> B(2-Plan)
B(2-Plan) --> C(3-Apply)
C(3-Apply) --> D(4-Destroy)
D(4-Destroy) -- Remove the .terraform --> A(1-Init)
```
1+2+3 --> Launch the infra on the cloud<br>
2+3 --> Apply changes to the infra<br>
4 --> Delete the infra<br>

**Init**
The init command verify and build your terraform config locally.<br>
Before launching the init command you have to rm the `.terraform` on the terraform directory.<br>
And after:
`terraform init -backend-config=<path-to-your-backend.conf> -var-file=<path-to-your-terraform.tfvars>`

**Plan**
The plan command use the local build of your **Init** and compare it with your AWS instance on the cloud.<br>
`terraform plan -var-file=<path-to-your-terraform.tfvars>`

**Apply**
The apply command deploy the infra builded and checked in your AWS cloud.<br>
`terraform apply -var-file=<path-to-your-terraform.tfvars>`

**Destroy**
`terraform destroy -var-file=<path-to-your-terraform.tfvars>`

## The arborescence

All your files are listed in the file explorer. The armature of your terraform config is splitted in 2 parts: VPC and APP.<br>

**VPC**
It allows you to deploy an entire VPC on the cloud with a complete configuration.<br>
```mermaid
graph LR
A(VPC) --> B(TERRAFORM)
A(VPC) --> C(MASTER)
```
The `terraform` folder contain all the files needed by terraform for creating your VPC.<br>
The `master` folder contain all the config files for changing the informations of your VPC.<br>

**APP**
It allows you to deploy your APP on AWS with an ECS, ECR, LB, RDS, S3 and a complete configuration.<br>
```mermaid
graph LR
A(APP) --> B(BACKEND)
A(APP) --> C(FRONTEND)
B(BACKEND) --> D(TERRAFORM)
B(BACKEND) --> E(PREPROD)
C(FRONTEND) --> F(TERRAFORM)
C(FRONTEND) --> G(PREPROD)
```
The `backend` folder contain all the files needed for deploying your back (RDS, S3, ...).<br>
The `frontend` folder contain all the files needed for deploying the front (ECS, TaskDef, ECR, ...).<br>
The `terraform` folder contain all the files needed by terraform for creating your VPC.<br>
The `master` folder contain all the config files for changing the informations of your VPC.<br>